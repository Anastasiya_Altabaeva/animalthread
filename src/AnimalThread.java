public class AnimalThread extends Thread {
    private String name;
    private int priority;

    AnimalThread (String name, int priority){
        setName(name);
        setPriority(priority);
    }

    public void run() {
        for (int i = 0; i < 10000; i += 100) {
            if (getPriority() == 10) {
                setPriority(1);
            } else if (getPriority() == 1) {
                setPriority(10);
            }
                System.out.println(getName() + " пробежал расстояние в " + i + " метров");
            }
        }
    }

