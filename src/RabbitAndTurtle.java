public class RabbitAndTurtle {
    public static void main(String[] args) {
        AnimalThread rabbit = new AnimalThread("кролик",10);
        AnimalThread turtle = new AnimalThread("черепашка",1);
        rabbit.start();
        turtle.start();
    }
}
